var express = require('express');
var router = express.Router();
var Post = require('../models/post');
var Category = require('../models/category');
var Comment = require('../models/comment');

router.get('/', function(req, res, next) {
    Post.find().populate('category').sort({createdTime: 'descending'}).exec(function (err, posts) {
    Category.find({},function (err,categories) {
        Comment.find({}).limit(4).sort({createdTime:-1}).populate('user').exec(function (err,comments) {
            res.render('index', {posts: posts,categories: categories,comments:comments});
        });
    });
  })
});

module.exports = router;
