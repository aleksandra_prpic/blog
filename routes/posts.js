var express = require('express');
var multer = require('multer');
var fs = require('fs');
var router = express.Router();
var upload = multer({ dest: '/tmp/'});

var Post = require('../models/post');
var Category = require('../models/category');
var Comment = require('../models/comment');
var cats, catsModify;


router.get('/', function (req, res, next) {
    if (req.query.category) {
        Post.find({category: req.query.category}).sort({createdTime: 'descending'}).exec(function (err, posts) {
            Category.find({},function (err,categories) {
                Comment.find({}).limit(4).sort({createdTime:-1}).populate('user').exec(function (err,comments) {
                    res.render('index', {posts: posts,categories: categories,comments:comments});
                });
            });
        });
    }
    ;
});

router.get('/add', function (req, res, next) {
    Category.find({}, function (err, data) {
        cats = data;
        if (err) return res.status(500).end('An error occured');
        res.render('add', {categories: cats, post: ''});
    });
});

router.post('/add',ensureUserIsAdmin,upload.single('file'), function (req, res, next) {
    var errors = [];
    if (!req.body.title)
        errors.push('Title is required');
    if (!req.body.content)
        errors.push('Content is required');
    if (!req.body.category && req.body.categoryDropdown == "none")
        errors.push('Category is required')
    if (req.body.category && req.body.categoryDropdown != "none")
        errors.push('You can not choose and add category, only one option is allowed')
    var post = {
        title: req.body.title,
        content: req.body.content
    };
    if (errors.length != 0) {
        return res.render('add', {errors: errors, categories: cats, post: post});
    }

    if (req.body.category)
        var category = new Category({name: req.body.category.toLowerCase()});
    else if (req.body.categoryDropdown != "none")
        var category = new Category({name: req.body.categoryDropdown.toLowerCase()});

    Category.findOne({'name': category.name}, function (err, cat) {
        if (err) return res.status(500).end('An error occured');
        if (!cat) {
            category.save(function (err, data) {
                if (err) return res.status(500).end('An error occured');
                var post = new Post({
                    title: req.body.title,
                    content: req.body.content,
                    category: data._id,
                    image: req.file.filename
                });
                var file = req.app.locals.pubDir +'/public/uploads/' + req.file.filename;
                fs.rename(req.file.path, file, function(err) {
                    if (err) {
                        console.log(err);
                        res.send(500);
                    } else {
                        post.save(function (err, data) {
                            if (err) return res.status(500).end('An error occured');
                        });
                        res.redirect('/');
                    }
                });
            })
        } else {
            var post = new Post({
                title: req.body.title,
                content: req.body.content,
                category: cat._id,
                image: req.file.filename
            });
            var file = req.app.locals.pubDir +'/public/uploads/' + req.file.filename;
            fs.rename(req.file.path, file, function(err) {
                if (err) {
                    console.log(err);
                    res.send(500);
                } else {
                    post.save(function (err, data) {
                        if (err) return res.status(500).end('An error occured');
                    });
                    res.redirect('/');
                }
            });
        }
    })

});

router.get('/:postId', function(req, res, next) {
    Post.findById(req.params.postId).populate('category').exec(function (err, data) {
        if(err) return res.status(500).end('An error occured');
        if(!data) return res.status(404).end('no data found');
        Comment.find({post:req.params.postId}).populate('user').exec(function (err,comments) {
            data.comments = comments;
            console.log(data);
            res.render('post',  {post:data });
        });
    });

});

router.get('/:postId/modify',ensureUserIsAdmin, function (req, res, next) {
    Post.findById(req.params.postId, function (err, data) {
        if (err) return res.status(500).end('An error occured');
        Category.find({}, function (err, categories) {
            var localCategories = categories;
            var firstCategory = categories[0];
            for (var i = 0; i < categories.length; i++) {
                if (data.category.toString() == categories[i]._id.toString()) {
                    localCategories[0] = categories[i];
                    localCategories[i] = firstCategory;
                }
            }
            catsModify = localCategories;
            res.render('modify', {post: data, categories: localCategories});
        });
    });
});

router.post('/:postId/modify',ensureUserIsAdmin,upload.single('file'), function (req, res, next) {
    //Error validation array
    var errors = [];
    //Validate required title
    if (!req.body.title)
        errors.push('Title is required');
    //Validate required content
    if (!req.body.content)
        errors.push('Content is required');
    //Validate that only 1 category is choosen
    if (req.body.category && req.body.categoryDropdown != "none")
        errors.push('You can not choose and add category, only one option is allowed')
    //Intanciate post object to load in view if there were errors
    var post = {
        title: req.body.title,
        content: req.body.content,
        _id: req.body.id
    };

    //If error array is not empty render same view but with errors
    if (errors.length != 0) {
        return res.render('modify', {errors: errors, categories: catsModify, post: post});
    }

    //Check if category from text input is set
    if (req.body.category)
        var category = new Category({name: req.body.category.toLowerCase()});
    //Check if category from dropdown menu was changed
    else if (req.body.categoryDropdown != "none")
        var category = new Category({name: req.body.categoryDropdown.toLowerCase()});
    //If neither category from text input is set or categoryDropdown was not changed, keep current category
    else if (req.body.categoryDropdown == "none")
        var category = new Category({name: catsModify[0].name});

    //Try to find category in database
    Category.findOne({'name': category.name}, function (err, cat) {
        if (err) return res.status(500).end('An error occured');
        //True if there is not category from request in database, new category will be created
        if (!cat) {
            //Create new category
            category.save(function (err, data) {
                if (err) return res.status(500).end('An error occured');
                //Once we have category we can edit post
                Post.findById(req.body.id, function (err, post) {
                    if (err) return res.status(500).end('An error occured');
                    if (!post) return res.end('Cannot find that post');
                    post.title = req.body.title;
                    post.content = req.body.content;
                    post.category = data._id;
                    if(req.file){
                        var file = req.app.locals.pubDir +'/public/uploads/' + req.file.filename;
                        fs.rename(req.file.path, file, function(err) {
                            if (err) {
                                console.log(err);
                                res.send(500);
                            } else {
                                post.image = req.file.filename;
                                post.save(function (err, data) {
                                    if (err) return res.status(500).end('An error occured');
                                });
                                return res.redirect('/posts/' + data._id);
                            }
                        });
                    }else {
                        post.save(function (err, data) {
                            if (err) return res.status(500).end('An error occured');
                            res.redirect('/posts/' + data._id);
                        });
                    }
                });
            })
        } else {
            //In else block if category from request already exists in database
            //Once we have category we can edit post
            Post.findById(req.body.id, function (err, post) {
                if (err) return res.status(500).end('An error occured');
                if (!post) return res.end('Cannot find that post');
                post.title = req.body.title;
                post.content = req.body.content;
                post.category = cat._id;
                if(req.file){
                    var file = req.app.locals.pubDir +'/public/uploads/' + req.file.filename;
                    fs.rename(req.file.path, file, function(err) {
                        if (err) {
                            console.log(err);
                            res.send(500);
                        } else {
                            post.image = req.file.filename;
                            post.save(function (err, data) {
                                if (err) return res.status(500).end('An error occured');
                            });
                            return res.redirect('/posts/' + post._id);
                        }
                    });
                }else {
                    post.save(function (err, data) {
                        if (err) return res.status(500).end('An error occured');
                        res.redirect('/posts/' + post._id);
                    });
                }
            });
        }
    })
});

router.get('/:postId/delete',ensureUserIsAdmin, function (req, res, next) {
    Post.findById(req.params.postId, function (err, data) {
        if (err) return res.status(500).end('An error occured');
        if (!data)
            return res.end('Cannot find that post');
        data.remove(function (err) {
            if (err) return res.status(500).end('An error occured');
            res.redirect('/');
        });
    });
});

router.post('/:postId/comment',ensureUserIsLoggedIn,function (req,res,next) {
    if(!req.body.message)
        return res.status(500).end('Comment message is required');
    Post.findById(req.params.postId,function (err,data) {
        if (err) return res.status(500).end('An error occured while finding post');
        if (!data) return res.end('Cannot find that post');
        var comment = new Comment({
            message:req.body.message,
            user:req.user._id,
            post:req.params.postId
        });
        comment.save(function (err,comment) {
            if (err) return res.status(500).end('An error occured while saving comment');
            data.comments.push(comment._id);
            data.save();
            res.redirect('/posts/' + req.params.postId);
        });
    });
});

router.post('/:postId/like',ensureUserIsLoggedIn,function (req,res,next) {
    Post.findById(req.params.postId,function (err,post) {
        if (err) return res.status(500).end('An error occured while finding post');
        if (!post) return res.end('Cannot find that post');
        post.likes++;
        post.save(function (err) {
            if (err) return res.status(500).end('An error occured while liking post');
            return res.status(200).json({success:true});
        });
    });
});

function ensureUserIsAdmin(req,res,next) {
    if(!req.isAuthenticated() || !req.user.isAdmin){
        return res.status(401).send('You are not authorized to do this.');
    }
    next();
}

function ensureUserIsLoggedIn(req,res,next) {
    if(!req.isAuthenticated())
        return res.status(401).send('You are not authorized to do this.');
    next();
}


module.exports = router;