$(document).ready(function () {
    $('#share').click(function () {
        $.ajaxSetup({cache: true});
        $.getScript('//connect.facebook.net/en_US/sdk.js', function () {
            FB.init({
                appId: '1517835104942438',
                version: 'v2.7' // or v2.1, v2.2, v2.3, ...
            });
            FB.ui(
                {
                    method: 'share',
                    display: 'popup',
                    href: window.location.href
                }, function (response) {
                });
        });
    });
});