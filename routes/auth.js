var express = require('express');
var router = express.Router();
var passport = require('passport');
var bcrypt = require('bcrypt');
var User = require('../models/user');




router.get('/register',redirectIfUserIsloggedIn, function (req, res, next) {
    res.render('register');
});

router.post('/register', function (req, res, next) {

    if (!req.body.username) {
        return res.status(500).json({message:'Username is required'});
    }
    if (!req.body.password || !req.body.password2) {
        return res.status(500).json({message: 'Both password fields are required'});
    }
    if (req.body.password != req.body.password2) {
        return res.status(500).json({message: 'Passwords do not match'});
    }

    User.findOne({username: req.body.username}, function (err, data) {
        if (err) return res.status(500).json({message: 'An error occured'});
        if (data) return res.status(500).json({message: 'User with that username already exists'});
        bcrypt.hash(req.body.password,10,function (err,hash) {
            if (err) return res.status(500).json({message: 'Error while hashing password'});

            var user = new User({
                username: req.body.username,
                password: hash
            });
            user.save();

            req.login(user, function(err){
                if(err) return next(err);
                return res.redirect('/');
            });
        });
    });
});

router.get('/login',redirectIfUserIsloggedIn, function (req, res, next) {
    console.log('isAuthenticated?' + req.isAuthenticated());
    res.render('login');
});

router.post('/login', function (req,res,next) {
    passport.authenticate('local', function(err, user, info) {
        if (err) { return next(err); }
        if (!user) return res.status(500).json({message:'Please double check your username and password'});
        req.logIn(user, function(err) {
            if (err) { return next(err); }
            return res.redirect('/');
        });
    })(req, res, next);
});

router.get('/logout',function (req,res,next) {
   req.logout();
   res.redirect('/');
});

function redirectIfUserIsloggedIn(req,res,next) {
    if(req.isAuthenticated())
        return res.redirect('/');

    next();
}

module.exports = router;