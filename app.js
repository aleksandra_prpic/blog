var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

mongoose.connect('localhost:27017/blog');

var index = require('./routes/index');
var posts = require('./routes/posts');
var auth = require('./routes/auth');

var User = require('./models/user');
passport.use(new LocalStrategy(
    function (username, password, done) {
        console.log('username:' + username)
        User.findOne({username: username}, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                console.log('did not find user')
                return done(null, false, {message: 'Incorrect username.'});
            }
            if (!User.validPassword(user.password, password)) {
                console.log('wrong password')
                return done(null, false, {message: 'Incorrect password.'});
            }
            console.log('ok')
            return done(null, user);
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});


var app = express();

//Helper methods for templates
app.locals.readMoreText = function (text, length) {
    if(text.toString().length > length)
        return readMoreText = text.substring(0,length) + "...";
    else
        return readMoreText = text.substring(0,length);
}
app.locals.moment = require('moment');


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

//Put whole user object from req to locals.user so it can be used application-wide
app.get('*', function (req,res,next) {
    res.locals.user = req.user || null;
    next();
});

app.locals.pubDir = __dirname;

app.use('/', index);
app.use('/posts', posts);
app.use('/auth', auth);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
