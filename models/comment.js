var mongoose = require('mongoose');

var schema = mongoose.Schema({
    message: {type: String, required: true},
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    post: {type: mongoose.Schema.Types.ObjectId, ref: 'Post'},
    createdTime: {type: Date, default: Date.now()}
});

module.exports = mongoose.model('Comment', schema);