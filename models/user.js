var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var schema = mongoose.Schema({
    username: {type: String, required: true},
    password: {type: String, required: true},
    isAdmin: {type: Boolean, required: true, default: false}
});

module.exports = mongoose.model('User', schema);

module.exports.validPassword = function (realPassword, candidatePassword) {
    return bcrypt.compareSync(candidatePassword,realPassword);
}