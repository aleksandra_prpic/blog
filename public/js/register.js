$(document).ready(function () {
    $('#registrationForm').submit(function (e) {
        e.preventDefault();
        var username = $("input[name='username']").val(),
            password = $("input[name='password']").val(),
            password2 = $("input[name='password2']").val();

        $.ajax({
            method: "POST",
            url: "/auth/register",
            data: {
                username: username,
                password:password,
                password2:password2}
        })
            .done(function(response) {
                window.location.href = window.location.origin;
            }).fail(function (response) {
                toastr.error(response.responseJSON.message);
            });
    });
});