$(document).ready(function () {
    var alreadyLiked = false;
    $('#likeBtn').click(function (e) {
        if(!alreadyLiked) {
            $.ajax({
                method: "POST",
                url: window.location + '/like'
            })
                .done(function (response) {
                    $('.likeCounter').text(parseInt($('.likeCounter').text()) + 1);
                    alreadyLiked = true;
                }).fail(function (response) {
                toastr.error('Error while liking post');
            });
        }
        else{
            toastr.error('You already liked this post!');
        }
    });
});