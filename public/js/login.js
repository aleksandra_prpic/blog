$(document).ready(function () {
    $('#loginForm').submit(function (e) {
        e.preventDefault();
        var username = $("input[name='username']").val(),
            password = $("input[name='password']").val();

        $.ajax({
            method: "POST",
            url: "/auth/login",
            data: {
                username: username,
                password:password
            }
        })
            .done(function(response) {
                window.location.href = window.location.origin;
            }).fail(function (response) {
            toastr.error(response.responseJSON.message);
        });
    });
});