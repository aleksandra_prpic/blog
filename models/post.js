var mongoose = require('mongoose');

var schema = mongoose.Schema({
    title: {type: String, required: true},
    content: {type: String, required: true},
    category: {type: mongoose.Schema.Types.ObjectId, ref: 'Category'},
    createdTime: {type: Date, default: Date.now},
    comments: [{type: mongoose.Schema.Types.ObjectId, ref: 'Comment'}],
    likes:{type:Number, default: 0},
    image:{type:String}
});

module.exports = mongoose.model('Post', schema);